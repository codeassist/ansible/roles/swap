|Branch|Status|
|------|:--------:|
|master|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/swap/badges/master/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/swap/commits/master)
|develop|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/swap/badges/develop/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/swap/commits/develop)

## Ansible Role
### **_swap_**

Manages additional swap file.

## Requirements

- Ansible 2.5 and higher

## Role Variables
```yaml
# TODO
```

## Dependencies

- sysctl
- mounts (_conditional_)

## License

MIT / BSD

## Author Information

This role was created by ITSupportMe, LLC in 2019.
